export default function GameInfo({ score1 = "-", score2 = "-" , currentPlayer, isVersusAI}) {

  let borderHighlightStyle = {
    outline: "solid 5px red"
  }

  return (
    <div id="game-info">
      <div className="score" style={(currentPlayer === 1) ? borderHighlightStyle : {outline: "none"}}>
      <div id="static-black-disk" className="static-disk"></div>
        <h2>Joueur 1</h2>
        <label>Score</label>
        <span id="score-first-player">{score1}</span>
      </div>
      <div className="score" style={(currentPlayer === 2) ? borderHighlightStyle : {outline: "none"}}>
      <div id="static-white-disk" className="static-disk"></div>
        <h2>{isVersusAI ? "🤖" : "Joueur 2"}</h2>
        <label>Score</label>
        <span id="score-second-player">{score2}</span>
      </div>
    </div>
  );
}
