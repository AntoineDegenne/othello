export default function Cell({
  cellID,
  cellStyle,
  cellValue,
  handleClick,
  handleHover,
  handleLeave,
}) {
  
  const diskStyleAttributes = {
    borderRadius: "50%",
    margin: "5%",
    width: "90%",
    height: "90%",
  };

  function diskStyle(value) {
    switch (value) {
      case 1:
        return { ...diskStyleAttributes, backgroundColor: "black" };
      case 2:
        return { ...diskStyleAttributes, backgroundColor: "white" };
      default:
        return null;
    }
  }

  return (
    <td
      id={cellID}
      className="cell"
      style={cellStyle}
      onClick={handleClick}
      onMouseOver={handleHover}
      onMouseOut={handleLeave}
    >
      <div style={diskStyle(cellValue)} onMouseOver={handleHover} onMouseOut={handleLeave}></div>
    </td>
  );
}
