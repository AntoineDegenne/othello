/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, useState } from "react";
import Cell from "./Cell";
import BoardLogic from "./BoardLogic";

// const debugGameOverState = [
//   [1, 2, 0, 2, 2, 2, 2, 2],
//   [2, 2, 2, 2, 1, 1, 1, 1],
//   [2, 2, 2, 2, 1, 1, 1, 1],
//   [2, 2, 2, 2, 1, 1, 1, 1],
//   [2, 2, 2, 2, 1, 1, 1, 1],
//   [2, 2, 2, 2, 1, 1, 1, 1],
//   [2, 2, 2, 2, 1, 1, 1, 1],
//   [2, 2, 0, 2, 1, 1, 1, 1],
// ];

export default function Board({ size, setScore, boardData, setBoardData }) {
  const [boardState, setBoardState] = useState(
    Array(size)
      .fill()
      .map(() => Array(size).fill(0))
  );

  // Permet de lancer une nouvelle partie
  useEffect(() => {
    if (boardData.start) {
      newGame();
      setBoardData({ ...boardData, start: false });
    }
  }, [boardData.start]);

  // Gère le tour de l'IA si elle est activée
  // Se lance à chaque changement de joueur ou que l'autre passe son tour
  useEffect(() => {
    if (boardData.ai === true && boardData.currentPlayer === 2) {
      const bestMove = BoardLogic.calculateBestMove(boardState);
      if (bestMove !== undefined) {
        setTimeout(() => {
          let newState = addDisk(bestMove.coord[0], bestMove.coord[1], 2);
          newState = flipOpponentDisks(newState, bestMove);

          updateBoard(newState, 1);
        }, 1000);
      }
    }
  }, [boardData.currentPlayer, boardData.pass]);

  // Réinitialise le board
  function newGame() {
    const newBoard = Array(size)
      .fill()
      .map(() => Array(size).fill(0));

    const middle = Math.floor(size / 2);

    newBoard[middle - 1][middle - 1] = 1;
    newBoard[middle][middle] = 1;
    newBoard[middle - 1][middle] = 2;
    newBoard[middle][middle - 1] = 2;

    updateBoard(newBoard, 1);
  }

  /**
   * Remet la couleur de fond transparente des cellules
   */
  function resetCellsBackground() {
    const cells = document.getElementsByClassName("cell");
    Array.from(cells).forEach(
      (cell) => (cell.style.backgroundColor = "transparent")
    );
  }

  /**
   * Ajoute le pion d'un joueurà l'endroit des coordonnées
   * @param {int} i Coordonnée des colonnes
   * @param {int} j Coordonnée des lignes
   * @param {int} player Joueur concerné
   * @returns
   */
  function addDisk(i, j, player) {
    let newState = [...boardState];
    newState[i][j] = player;
    return newState;
  }

  /**
   * Permet de retourner les pions adverses selon l'état du board et le coup
   * @param {int[][]} board
   * @param {*} chosenMove
   * @returns
   */
  function flipOpponentDisks(board, chosenMove) {
    chosenMove.disksToTurn.forEach((element) => {
      element.forEach((coord) => {
        board[coord[0]][coord[1]] = boardData.currentPlayer;
      });
    });

    return board;
  }

  /**
   * Assombri les cases où un coup est possible
   */
  function displayLegalMoves(legalMoves) {
    legalMoves.forEach((element) => {
      let cell = document.querySelector(
        "#i" + element.coord[0] + "j" + element.coord[1]
      );
      cell.style.backgroundColor = "rgba(10, 10, 150, 0.3)";
    });
  }

  /**
   * Gère le clique sur une case du board
   * @param {int} i Coordonée de la colonne
   * @param {int} j Coordonnée de la ligne
   */
  function handleClick(i, j) {
    // Est actif uniquement si c'est pas l'IA
    if (boardData.ai === false || boardData.currentPlayer === 1) {
      const opponent = BoardLogic.getOpponent(boardData.currentPlayer);

      const localCoord = [i, j];
      const legalMoves = BoardLogic.calculateLegalMoves(
        boardState,
        boardData.currentPlayer
      );
      const chosenMoveIndex = legalMoves.findIndex(
        (element) =>
          JSON.stringify(element.coord) === JSON.stringify(localCoord)
      );

      // chosenMoveIndex correspond à l'index du coup choisi parmis la liste des coups possibles
      // Si -1, le coup n'est pas possible
      if (chosenMoveIndex > -1) {
        let newState = addDisk(i, j, boardData.currentPlayer);
        newState = flipOpponentDisks(newState, legalMoves[chosenMoveIndex]);

        updateBoard(newState, opponent);
      }
    }
  }

  // Logique pour mettre à jour la board
  function updateBoard(boardState, player) {
    let newBoardData = boardData;
    let newPlayer = player;

    let legalMoves = BoardLogic.calculateLegalMoves(boardState, player);

    // Si il n'y a pas de move possible, le tour est sauté
    if (legalMoves.length === 0) {
      newPlayer = BoardLogic.getOpponent(player);
      legalMoves = BoardLogic.calculateLegalMoves(boardState, newPlayer);
      newBoardData = { ...newBoardData, pass: true };
      // Si il n'y a pas de move possible pour l'adversaire non plus, la partie est terminée
      if (legalMoves.length === 0) {
        newBoardData = { ...newBoardData, gameOver: true };
      }
    } else {
      newBoardData = { ...newBoardData, pass: false };
    }

    resetCellsBackground();
    displayLegalMoves(legalMoves);
    setBoardState(boardState);
    setBoardData({ ...newBoardData, currentPlayer: newPlayer });
    setScore(BoardLogic.countDisks(boardState));
  }

  // Gere la couleur des borders du mouse hover
  function handleCellMouseEvent(e, color) {
    // Si la cible n'a pas d'enfant, c'est le disk et on vise la bordercolor du parent cell
    if (e.target.children.length === 0) {
      e.target.parentElement.style.borderColor = color;
      // Sinon, c'est la cell elle-meme
    } else {
      e.target.style.borderColor = color;
    }
  }

  // Génère les lignes du boards
  function tableRows() {
    let tableRows = [];
    for (let i = 0; i < size; ++i) {
      let tableRow = [];
      for (let j = 0; j < size; ++j) {
        let cellStyle = {
          height: "50px",
          width: "50px",
          borderStyle: "solid",
          borderWidth: "1px",
          borderColor: "#636363",
        };
        if (i === 0 && j === 0) {
          cellStyle.borderTopLeftRadius = "5px";
        } else if (i === 0 && j === size - 1) {
          cellStyle.borderTopRightRadius = "5px";
        } else if (i === size - 1 && j === 0) {
          cellStyle.borderBottomLeftRadius = "5px";
        } else if (i === size - 1 && j === size - 1) {
          cellStyle.borderBottomRightRadius = "5px";
        }

        tableRow.push(
          <Cell
            key={"i" + i + "j" + j}
            cellID={"i" + i + "j" + j}
            cellStyle={cellStyle}
            cellValue={boardState[i][j]}
            handleClick={() => handleClick(i, j)}
            handleHover={(e) => handleCellMouseEvent(e, "red")}
            handleLeave={(e) => handleCellMouseEvent(e, "#636363")}
          />
        );
      }
      tableRows.push(tableRow);
    }
    return tableRows;
  }

  const tableRowsList = tableRows().map((tableRow, index) => {
    return <tr key={"row" + index}>{tableRow}</tr>;
  });

  const tableStyle = {
    backgroundColor: "rgb(81, 149, 72)",
    borderSpacing: "0px",
    borderRadius: "5px",
  };

  return (
    <>
      <table style={tableStyle}>
        <tbody>{tableRowsList}</tbody>
      </table>
    </>
  );
}
