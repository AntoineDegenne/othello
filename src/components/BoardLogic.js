class BoardLogic {

  static getOpponent(player) {
    return player === 1 ? 2 : 1;
  }

  /**
   * Compte le nombre de pions noirs et le nombre de pions blancs sur le board
   * @param {int[][]} boardState
   * @returns
   */
  static countDisks(boardState) {
    let whitesLeft = 0;
    let blacksLeft = 0;
    boardState.forEach((row) => {
      row.forEach((element) => {
        if (element === 1) {
          blacksLeft += 1;
        } else if (element === 2) {
          whitesLeft += 1;
        }
      });
    });

    return {
      black: blacksLeft,
      white: whitesLeft,
    };
  }

  /**
   * Calcule quels pions adverses peuvent être retournés selon le joueur actuel et les coordonnées du coup
   * @param {int[][]} boardState Le tableau 2D représentant l'état du plateau
   * @param {int} currentPlayer Le joueur qui a fait le coup
   * @param {int} i La ligne du coup
   * @param {int} j La colonne du coup
   * @returns Un array des coordonnés des pions adverses à retourner s'ils existent, sinon un array vide
   */
  static calculateDisksToTurn(boardState, currentPlayer, i, j) {
    let disksToTurn = [];
    // Table des vecteurs de toutes les directions
    const directionTable = [
      [-1, -1],
      [-1, 0],
      [-1, 1],
      [0, -1],
      [0, 0],
      [0, 1],
      [1, -1],
      [1, 0],
      [1, 1],
    ];
    const len = boardState.length;
    const opponentPlayer = this.getOpponent(currentPlayer);

    // Pour chaque direction, on enregistre les coordonnées des disques à retourner dans un array
    disksToTurn = directionTable.map((dir) => {
      let k = 1;
      let x = i + k * dir[0]; // Coordonnée de la colonne dans le sens de la direction
      let y = j + k * dir[1]; // Coordonnée de la ligne dans le sens de la direction
      let directionState = []; // Liste des coordonnées des pions à retourner
      let validateMove = false; // N'est vrai que si le mouvement est validé

      // Ne doit pas dépasser les limites du board
      while (x > -1 && y > -1 && x < len && y < len) {
        // Si la case est vide, aucun disque ne doit être retourné dans cette direction
        if (boardState[x][y] === 0) {
          break;

          // Si la case appartient au joueur actuel, les disques de ce mouvement peucvent être retourné et le mouvement est fini
        } else if (boardState[x][y] === currentPlayer) {
          validateMove = true;
          break;

          // Si la case appartient au joueur adverse, les disques entrent dans l'array des disques à retourner
          // et on va chercher le disque suivant dans le mouvement
        } else if (boardState[x][y] === opponentPlayer) {
          directionState.push([x, y]);
          k += 1;
          x = i + k * dir[0];
          y = j + k * dir[1];
        }
      }

      // Si le mouvement est validé et qu'il y a des disques à retourner, on l'ajoute dans l'array à retourner
      if (validateMove === true && directionState.length > 0) {
        return directionState;
      } else {
        return null;
      }
    });
    return disksToTurn.filter(element => element !== null);
  }

  /**
   * Calcule les mouvements possibles sur tout le board selon le joueur en cours
   * @param {int[][]} boardState
   * @param {int} currentPlayer
   * @returns
   */
  static calculateLegalMoves(boardState, currentPlayer) {
    let legalMoves = [];

    const len = boardState.length;

    // Pour chaque case de la board
    for (let i = 0; i < len; i++) {
      for (let j = 0; j < len; j++) {
        // Si la case est vide, on calcule les pions à retourner dans l'hypothèse où le coup serait joué ici
        if (boardState[i][j] === 0) {
          const disksToTurn = this.calculateDisksToTurn(
            boardState,
            currentPlayer,
            i,
            j
          );

          // S'il existe des pions à retourner, le coup est ajouté dans l'array des coups possibles sous forme d'objet
          // avec les coordonnées de la case où le point doit être placé pour faire le mouvement
          // et un array des coordonnées de tous les pions à retourner dans ce mouvement
          if (disksToTurn.length > 0) {
            let legalMove = {
              coord: [i, j],
              disksToTurn: disksToTurn,
            };
            legalMoves.push(legalMove);
          }
        }
      }
    }

    return legalMoves;
  }

  /** Renvoie un objet représentant le meilleur coup à faire avec ses coord et ses disksToTurn
   *
   * @param {int[][]} boardState
   * @returns Le "meilleur" move à faire parmis tous les moves possibles
   */
  static calculateBestMove(boardState) {
    const len = boardState.length;
    const legalMoves = this.calculateLegalMoves(boardState, 2); // AI sera toujours joueur 2

    let cornerMove;
    let bestMove;

    // La liste des coups possibles doit exister
    if (legalMoves.length > 0) {
      bestMove = legalMoves[0];

      legalMoves.forEach((move) => {
        // Si le coup possible est dans un coin, il prend la priorité
        if (
          (move.coord[0] === 0 || move.coord[0] === len - 1) &&
          (move.coord[1] === 0 || move.coord[1] === len - 1)
        ) {
          cornerMove = move;

          // Sinon, le coup avec le plus de pions à retourner est priorisé
        } else if (move.disksToTurn.length > bestMove.disksToTurn.length) {
          bestMove = move;
        }
      });
    }
    return cornerMove !== undefined ? cornerMove : bestMove;
  }
}

export default BoardLogic;
