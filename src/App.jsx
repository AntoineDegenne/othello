import "./normalize.css";
import "./App.css";
import GameInfo from "./components/GameInfo";
import Board from "./components/Board";
import { useState } from "react";

function App() {
  const [score, setScore] = useState({});
  const [boardData, setBoardData] = useState({});
  const [displayBoard, setDisplayBoard] = useState(false);

  const boardSize = 8;

  /**
   * Affiche le message du gagnant en fonction du score
   * 
   */
  function displayGameOverLabel() {
    if (boardData.gameOver === true) {
      if (score.black > score.white) {
        return (
          <div className="shadowBox">
            <p
              id="winner-black"
              className="win-label rainbow rainbow_text_animated"
            >
              Winner is Black !!!
            </p>
          </div>
        );
      } else if (score.black < score.white) {
        return (
          <div className="shadowBox">
            <p
              id="winner-white"
              className="win-label rainbow rainbow_text_animated"
            >
              Winner is White !!!
            </p>
          </div>
        );
      } else {
        return (
          <div className="shadowBox">
            <p id="draw" className="win-label rainbow rainbow_text_animated">
              EGALITE !!!
            </p>
          </div>
        );
      }
    }
  }

  /**
   * Remet les compteurs à zéro et affiche le board si c'est pas déjà fait
   * @param {boolean} aiState Si l'IA est activée ou pas
   */
  function startNewGame(aiState) {
    setScore({
      black: 0,
      white: 0,
    });
    setBoardData({ currentPlayer: 1, start: true, ai: aiState });
    setDisplayBoard(true);
  }

  return (
    <div className="App">
      <header>
        <h1>Othello</h1>
        <p>
          Les règles du jeu sont disponibles en cliquant{" "}
          <a href="https://www.regledujeu.fr/othello/">ici</a>
        </p>
      </header>
      <GameInfo
        score1={score.black}
        score2={score.white}
        currentPlayer={boardData.currentPlayer}
        isVersusAI={boardData.ai}
      />
      <div id="game-content">
        {displayBoard && (
          <Board
            id="board"
            size={boardSize}
            setScore={setScore}
            boardData={boardData}
            setBoardData={setBoardData}
          />
        )}
      </div>
      <div className="button-container">
        <button className="new-game-button" onClick={() => startNewGame(false)}>
          VS. player
        </button>
        <button className="new-game-button" onClick={() => startNewGame(true)}>
          VS. AI
        </button>
      </div>
      {boardData.gameOver && displayGameOverLabel()}
    </div>
  );
}

export default App;
